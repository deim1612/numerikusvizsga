%Fogarassy Norbert, fnim1621, 533

function L = Cholesky(A)

    n=length(A);
    L=eye(n);
    if issymmetric(A)
        for i=1:n
            for j=1:i
                s=0;
                ss=0;
                for k=1:(j-1)
                    s=s+L(j,k)*L(i,k);
                    ss=ss+L(i,k)*L(i,k);
                end
                if (i~=j)
                    L(i,j)=1/L(j,j)*(A(i,j)-s);
                else
                    x=A(i,i)-ss;
                    if x>0
                        L(i,j)=sqrt(x);
                    else
                        disp('A matrix nem pozitiv definitiv')
                    end
                end
            end
        end
    else
    disp('A matrix nem szimmetrikus')
    end
    
end