%Fogarassy Norbert, fnim1621, 533

function [L,U,P]=LUP(A)
    n=length(A);
    L=eye(n);
    P=eye(n);
    
    for i=1:n
        [m,row] = max(abs(A(i:n,i)));
        row=row+i-1;
       
        L([i row],:)=L([row i],:);
        A([i row],:)=A([row i],:);
        P([i row],:)=P([row i],:);
        
        for k=i+1:n
            L(k,i)=A(k,i)/A(i,i);
            for j=i:n
                A(k,j)=A(k,j)-A(i,j)*L(k,i);
            end
        end
    end
        
   L = tril(L);
   for i=1:n
       L(i,i)=1;
   end
   U = triu(A);
   P
   L
   U
end