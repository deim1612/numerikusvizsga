function ans = Romberg(f,a,b,eps,maxit)


 Q = zeros(maxit);
 h = b-a;
 ans = 0 ; 
 
 Q(1,1)=(b-a)/2*(f(a)+f(b));
 
 hatvketto=2;
 
    for k=2:maxit
        s=0;
            for i=1:2^(k-2)
                s=s+f(a+(2*i-1)*h/hatvketto);
            end
        Q(k,1)=1/2*Q(k-1,1)+h/(2^(k-1))*s;
        hatvketto=hatvketto*2;
    end
 
   hatvnegy=4;
    
  for i=2:maxit
      for j=2:i
        Q(i,j)=(hatvnegy*Q(i,j-1)-Q(i-1,j-1))/(hatvnegy-1);
      end
      hatvnegy=hatvnegy*4;
      if(abs( Q(i-1,i-1)-Q(i,i))<eps)
          ans=Q(i,i)
          return
       end
  end
   
   error("Tul sok iteracio");
 
 end