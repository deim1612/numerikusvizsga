function SplineRajz(a,b)
    X = linspace(a,b,20);
    x = linspace(a,b,1000);
    n = length(x);
    Spline = zeros(1,n);
    f = @(X)1./(1+X.^2);
    Xf = f(X);

    for i=1:n
        Spline(i) = SplineInterpol(X,Xf,x(i));
    end

    hold on;
    plot(x,Spline,'-r');
    plot(x,f(x),'-g');

    axis([a,b,a,b]);
end