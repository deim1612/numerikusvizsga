function [Er] = SplineInterpol(X,Xf,x)

    n = length(X);
    sz = find(x>=X);

    if (n==length(sz))
       i = n-1;
    else i = length(sz);
    end
    
    C = Spline(X,Xf);
    Er = C(1,i) + C(2,i)*(x-X(i)) + C(3,i)*(x-X(i))^2 + C(4,i)*(x-X(i))^3;

end