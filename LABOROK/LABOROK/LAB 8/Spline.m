function [C] = Spline(X,Xf)
    n = length(X);
    lambda = zeros(1,n);
    m = zeros(1,n);
    d = zeros(1,n);
    M = zeros(1,n);
    C = zeros(4,n);
    
    for j=1:n-1
        h(j+1) = X(j+1)-X(j);
    end
    
    lambda(1)=1;
    d(1)=0;
    m(n)=1;
    d(n)=0;
    
    m(1)=0;
    lambda(n)=0;
    
    for j=2:n-1
        lambda(j) = h(j+1)/(h(j)+h(j+1));
        m(j) = 1 - lambda(j);
        d(j) = 6/(h(j)+h(j+1))*((Xf(j+1)-Xf(j))/h(j+1))-((Xf(j)-Xf(j-1))/h(j));
    end
    
    A = [m; 2*ones(1,n); lambda];
    tri = spdiags(A', -1:1 , n , n);
    M = tri\d';
    
    for j=1:n-1
        C(1,j) = Xf(j);
        C(2,j) = (Xf(j+1) - Xf(j))/h(j+1) -((2*M(j)+M(j+1))/6)*h(j+1);
        C(3,j) = M(j)/2;
        C(4,j) = (M(j+1)-M(j))/(6*h(j+1));
    end
    
end