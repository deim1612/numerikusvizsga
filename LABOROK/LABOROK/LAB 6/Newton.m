%[Lx] = Newton([1:5], [1:5].^3, 2.5)

%Tablazat elso oszlopaba beteszem Fx ertetekeket, majd ezek alapjan
%szamolja a foatlo folotti reszt
%ez utan x-minden X szerint e keplet s a tablazat elso sorabol az ertekek

function [Lx] = Newton(X, fX, x)

n=length(X);
tablazat=zeros(n, n);
tablazat(:,1)=fX;
%tablazat

for j=2:n
    for i=1:(n-j+1)
        %i,j
        %X(i+j-1)-X(i)
        %tablazat
        tablazat(i, j)=(tablazat(i+1, j-1)-tablazat(i, j-1))/(X(i+j-1)-X(i));
        %tablazat
    end
end

Lx=tablazat(1, n);

tablazat
%Lx

for i=n-1:-1:1
    %Lx
    %x-X(i)
    %tablazat(1,i)
    %tablazat
    Lx=Lx.*(x-X(i))+tablazat(1, i);
    %Lx
end

end
