%NewtonRajz(-5,5,21,@(x)1./(1+x.^2))
function NewtonRajz(a,b,n,f)

X = linspace(a, b, n);
X2 = linspace(a, b, 100000);

hold on;
plot(X2, f(X2), 'g');

LfX=Newton(X,f(X),X2);
plot(X2, LfX, 'r');

axis([a,b,a,b]);

end