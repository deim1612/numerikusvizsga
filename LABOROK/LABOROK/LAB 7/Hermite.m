%X = [1.3 1.6 1.9]
%fX = [0.6200860 0.4554022 0.2818186]
%f1X = [-0.5220232 -0.5698959 -0.5811571]
%x = 1.5
%H5(1.5) = 0.5118277
%Hermite([1.3 1.6 1.9],[0.6200860 0.4554022 0.2818186],[-0.5220232 -0.5698959 -0.5811571],1.5)

function H=Hermite( X, fX, dfX, x)
    m = length(X);
    z = zeros(1,2*m);
    Q = zeros(2*m,2*m);
    
    for i=1:m
        z(2*i) = X(i);
        z(2*i-1) = X(i);
        
        Q(2*i,1) = fX(i);
        Q(2*i-1,1) = fX(i);
        
        Q(2*i,2) = dfX(i);
        if(i>1)
            Q(2*i-1,2) = (Q(2*i-1,1)-Q(2*i-2,1))/(z(2*i-1)-z(2*i-2));
        end
    end
    
    for i=3:2*m
        for j=3:i
            Q(i,j) = (Q(i,j-1)-Q(i-1,j-1))/(z(i)-z(i-j+1));
        end
    end
    %Q
   
    H = Q(1,1);
    for i = 2:2*m
        sz = 1;
        for j=1:i-1
            sz = sz .* (x-z(j));
        end
        H = H + Q(i,i).*sz;
    end
end