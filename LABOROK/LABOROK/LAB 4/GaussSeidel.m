function [x,it] = GaussSeidel(A, b)
    
    n = length(A);
    L = triu(A) - A;
    U = tril(A) - A;
    D = A + L + U;
    P = D - L;
    Q = U;
    B = P^(-1) * Q;
    b = b';
    
    for i=1:n
        ossz=0;
        for j=1:n
            if(i~=j)
                ossz=ossz+abs(A(i,j));
            end
        end
        if(abs(A(i,i))<=ossz)
            error('Nem dominans a soronkent a foatlo!');
        end
    end
    
    x = zeros(n, 1);
    f= P^(-1)*b;
    felt=(1-norm(B))/norm(B)*eps;
    it=0;
    
    while(true)
        xk=B*x+f;
        if (norm(xk-x)<(felt))
            break;
        end
        x=xk;
        it=it + 1;
    end
end