%2. �Irjunk egy MatLab fu�ggv�enyt melynek bemenete az (U,b) p�aros, kimenete pedig az x megold�asvektor.
%(function x=UTriangSolve(U,b))

function x=UTriangleSolve(A,b)
	s=size(A,1);
	x(s)=b(s)/A(s,s);
	for i=s-1:-1:1
		sum=0;
		for j=i+1:s
			sum=sum+A(i,j)*x(j);
		end
		x(i)=(b(i)-sum)/A(i,i);
	end
end