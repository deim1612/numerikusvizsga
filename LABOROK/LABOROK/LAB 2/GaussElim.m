%1.�Irjunk egy olyan MatLab fu�ggv�enyt, melynek bemenete az (A,b) p�aros, 
%kimenete pedig [U,c], ha el tudta v�egezni az elimin�aci�ot, 
%ellenkez?o esetben hibau�zenet. (function [U,c]=GaussElim(A,b))

function [X,y] = GaussElim(A,b)
n = length(A);

	for k=1:n-1
		for i=k+1:n
			max = abs(A(k,k));
			maxIndex = k;
			
			for j=k+1:n
				if (abs(A(j,k))>max)
					max = abs(A(j,k));
					maxIndex = j;
				end
			end
			
				sorA = A(k,:);
				A(k,:) = A(maxIndex,:);
				A(maxIndex,:) = sorA;
				sorB = b(k);
				b(k) = b(maxIndex);
				b(maxIndex) = sorB;
				l=A(i,k)/A(k,k);
				b(i) = b(i) - l*b(k);
				for j=k:n
					A(i,j) = A(i,j) - l*A(k,j);
        		end
		end
	end
	X = A;
	y = b;
end