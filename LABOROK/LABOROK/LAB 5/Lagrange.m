function r= Lagrange(X, fX, x, eps)

    m=length(X);
    Q=zeros(m,m);
    [~,b]=sort(abs(X - x));
    X=X(b);
    fX=fX(b);
    Q(:,1)=fX;
    %Q
    %b
    
    for i=2:m
        for j=1:i-1
            Q(i,j+1)=(Q(j,j)*(X(i)-x)-Q(i,j)*(X(j)-x))/(X(i)-X(j));
        end
        
        if(abs(Q(i,i)-Q(i-1,i-1))<eps)
            fprintf('L%df(x)=%f\n',i-1,Q(i,i));
            r=Q(i,i);
            return;
        end
    end
    disp(Q);
    disp('Adat hiba');
end