function [ret] = mauSin(x,eps)
    
ret=0;
temp=eps+10;
i=0;

while abs(ret-temp)>eps
    if (mod(i,2)==0) rema=-1;
    else rema=1;
    end

    temp=ret;
    value=((-1)*rema)*((x^(2*i+1)/factorial(2*i+1)));
    ret=ret+value;
    i=i+1;
end
end