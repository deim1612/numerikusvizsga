function domMat(A)

n=size(A);
output=true;

if size(A,1)~=size(A,2)
    disp("Nem negyzetes.");
end

for i=1:n
    ossz=sum(abs(A(i))- abs(A(i,i)));
    if abs(A(i,i))<ossz 
        output=false;
        break;
    end
end

if (output==true)
    disp("Dominans atloju");
else disp("Nem dominans atloju");
end
