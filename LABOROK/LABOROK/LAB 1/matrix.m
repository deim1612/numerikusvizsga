%Fogarassy Norbert, fnim1621, 533, LAB1

function matrix()
    v = [32; 23; 33; 31;];
    m = [10 7  8  7;7 5  6  5;8 6 10  9;7 5  9 10;];
    pv = [32.1; 22.9; 33.1; 30.9;]
    pm = [10 7  8.1  7.2; 7.08 5.04  6  5; 8 5.98 9.89  9; 6.99 4.99  9 9.89;];
    
    im=inv(m);
    fprintf('Inverz:\n');
    disp(im);
    
    dm=det(m);
    fprintf('Det:\n');
    disp(dm);
    
    eredeti=linsolve(m,v);
    fprintf("Megoldas:\n");
    disp(eredeti);
    
    pmm = linsolve(pm, v);
    fprintf("Perturbalt matrix megoldas:\n");
    disp(pmm);
    
    pvm = linsolve(m, pv);
    fprintf("Perturbalt vektor megoldas:\n");
    disp(pvm);
    
    fprintf('Hiba matrix:\n%d\n',norm(pm-m)/norm(m));
    fprintf('Hiba perturbalt matrix:\n%d\n',norm(pmm-eredeti)/norm(eredeti));
    fprintf('Hiba vektor:\n%d\n',norm(pv-v)/norm(v));
    fprintf('Hiba perturbalt vektor:\n%d\n',norm(pvm-eredeti)/norm(eredeti));
    
end