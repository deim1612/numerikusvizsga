%Fogarassy Norbert, fnim1621, 533, LAB1

function [ret] = mauSin(x,ep)

ret=0;
temp=ep+1;
i=0;

x=mod(x,pi*2)

while abs(ret-temp)>ep
    if (mod(i,2)==0) rema=-1;
    else rema=1;
    end

    temp=ret;
    value=((-1)*rema)*((x^(2*i+1)/factorial(2*i+1)));
    ret=ret+value;
    i=i+1;
end
end