%Fogarassy Norbert, fnim1621, 533, LAB1

function vander(n)
if (n<9 || n>15)
    fprintf("Hibas parameter.")
else 
        fprintf("%d\n",n);
        v1=-1:(2/n):1;
        v2=1./[1:n];
        m1=func(v1);
        m2=func(v2);
        c1 = norm(inv(m1),inf)*norm(m1,inf);
        fprintf ("%f\n",c1);
        c2 = norm(inv(m2),inf)*norm(m2,inf);
        fprintf ("%f\n",c2);
       % disp(m1);
       % disp(m2);
end
end