%Fogarassy Norbert, fnim1621, 533, LAB1

function [ret] = func(v)
    n=length(v);
    vv = zeros(n,n);   
    for i=1:n
        vv(:,i)=v.^(i-1);
    end
    ret=vv;
end