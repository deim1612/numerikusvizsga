%Fogarassy Norbert, fnim1621, 533, LAB1

function ret=eps()
    
    epss = 1;
    while (1+(epss/2))>1
        epss=epss/2;
    end
    ret=epss;
end