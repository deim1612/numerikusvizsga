function [T] = newtonRaphson(N,f,fd,x0,ep)
    T= zeros(1,N);
    T(1)=x0;
    i=2;
    T(i)=T(i-1)-f(T(i-1))/fd(T(i-1));
    while(abs(T(i)-T(i-1))>ep)
        if(i>N)
            error('Too many iterations');
        end
        i=i+1;
        T(i)=T(i-1)-f(T(i-1))/fd(T(i-1));
    end
    T = T(T ~= 0);
    i
end