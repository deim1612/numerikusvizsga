function [xVec,i] = hur(func, x0, x1, ep, N)

xVec = zeros(1, N);
xVec(1) = x0;
xVec(2) = x1;
i = 2;
x = linspace(x0,x1,10);
fx = func(x);

plot(x,fx);
hold on;
plot([x0,x1],[0,0]);
pause(1)
plot([x0,x1],[func(x0),func(x1)]);
pause(1)

while (abs(xVec(i) - xVec(i - 1)) > ep)
    if (i > N)
       error('Too many iterations');
    end
    prev = xVec(i);
    prev2 = xVec(i -1);
    xVec(i+1) = prev - (func(prev) * (prev - prev2)) / (func(prev) - func(prev2));
    plot([xVec(i),xVec(i+1)],[func(xVec(i)),func(xVec(i+1))]);
    pause(1);
    i=i+1;
end

i
xVec = xVec(xVec ~= 0);